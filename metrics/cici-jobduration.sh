#!/bin/bash

for i in {1..31}; do
    mydate=$(date -I -d "2018-12-31 +$i days")
    my_url="http://10.219.1.5:9090/api/v1/query?query=histogram_quantile(0.99%2C%20sum(rate(job_queue_duration_seconds_bucket%7Benvironment%3D%22gprd%22%2C%20shared_runner%3D%22true%22%7D%5B1d%5D))%20by%20(le))&time=${mydate}T20:00:00Z"
    res=$(curl -s ${my_url})
    echo "${mydate} , ${res}"
done