#!/bin/bash

for i in {1..30..7}; do
    mydate=$(date -I -d "2019-01-03 +$i days")
    totalq='sum(increase(gitlab_runner_jobs_total%7Bjob%3D%22shared-runners-gitlab-org%22%7D%5B7d%5D))'
    failedq='sum(increase(gitlab_runner_failed_jobs_total%7Bjob%3D%22shared-runners-gitlab-org%22%2C%20failure_reason%3D%22runner_system_failure%22%7D%5B7d%5D))'
    errorq='sum(increase(gitlab_runner_errors_total%7Bjob%3D%22shared-runners-gitlab-org%22%2C%20level!%3D%22warning%22%7D%5B7d%5D))'
    tot="http://10.219.1.10:9090/api/v1/query?query=${totalq}&time=${mydate}T20:00:00Z"
    faild="http://10.219.1.10:9090/api/v1/query?query=${failedq}&time=${mydate}T20:00:00Z"
    errord="http://10.219.1.10:9090/api/v1/query?query=${errorq}&time=${mydate}T20:00:00Z"
    rest=$(curl -s ${tot})
    resf=$(curl -s ${faild})
    rese=$(curl -s ${errord})
    echo "${mydate} , ${rest} , ${resf} , ${rese}"
done